let randomizer;

function setup(){
  resetSketch();
}

function draw(){
  randomizer = random(1,20);
  noStroke();
  ellipse(mouseX,mouseY,randomizer,randomizer);
}

function resetSketch(){
  createCanvas(windowWidth,windowHeight);
  background(255,10);
  fill(random(200),100,random(100),150);
}

function mousePressed(){
  resetSketch();
}
